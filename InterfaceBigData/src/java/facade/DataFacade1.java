package facade;

import data.Data1;
import unit.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class DataFacade1 extends AbstractFacade<Data1>{
	@PersistenceContext(unitName = "InterfaceBigDataPU")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public DataFacade1() {
		super(Data1.class);
	}
}