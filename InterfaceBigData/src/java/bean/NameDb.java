package bean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@ManagedBean
@RequestScoped
public class NameDb implements Serializable{
    private String name;
    
    public NameDb() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @PostConstruct
    public void init(){
        name = getNameDb();
    }
    
    public String getNameDb(){
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();    	
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document= builder.parse(new File("/home/anna/NetBeansProjects/InterfaceBigData/setup/glassfish-resources.xml"));
            Element resource = document.getDocumentElement();	
            NodeList jdbclist = resource.getChildNodes();
            int nbJdbc = jdbclist.getLength();
            for(int i=0; i<nbJdbc;i++){
                if(jdbclist.item(i).getNodeName().equals("jdbc-connection-pool") && jdbclist.item(i).getNodeType() == Node.ELEMENT_NODE){
                    NodeList propList = jdbclist.item(i).getChildNodes();
                    int nbProp = propList.getLength();
                    for(int j = 0; j<nbProp; j++) {
                        if(propList.item(j).getNodeType() == Node.ELEMENT_NODE){
                            Element prop = (Element) propList.item(j);
                            if(prop.getAttribute("name").equals("DatabaseName")){
                                return prop.getAttribute("value");
                            }
                        }
                    }
                }
            }	
        }
        catch (final ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (final SAXException e) {
            e.printStackTrace();
        }
        catch (final IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String newDataBase(){
        return "case1";
    }
    
    public String selectData(){
        return "case2";
    }
    
    public String seeData(){
        return "case3";
    }
}

