package bean;

import tool.WriteFile;
import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class ConnectDb implements Serializable{
    private String user;
    private String password;
    private String port;
    private String serverDb;
    private String ip;
    private String nameDb;
    
    public ConnectDb() {
    }
    
    public String getIp() {
        return ip;
    }

    public String getNameDb() {
        return nameDb;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    
    public void setNameDb(String nameDb) {
        this.nameDb = nameDb;
    }
    
    public String getServerDb() {
        return serverDb;
    }

    public void setServerDb(String serverDb) {
        this.serverDb = serverDb;
    }
    
    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getPort() {
        return port;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String connectDb(){
        WriteFile ws = new WriteFile();
        ws.writeRessources(serverDb, ip, port, nameDb, user, password);
        ws.writePersistence(nameDb);
        return "case1";    
    }
}
