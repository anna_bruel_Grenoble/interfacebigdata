package bean;

import tool.WriteFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import unit.HeaderFacade;

@ManagedBean
@RequestScoped
public class InformationDb implements Serializable{
    private List<String> table;
    private Map<String, List<String>> column;
    private List<String> keyTemp;
    private Map<List<String>,Boolean> check;
    @EJB
    private HeaderFacade header;
    
    public InformationDb(){
        table = new ArrayList<String>();
        column = new HashMap<String, List<String>>();
        check = new HashMap<List<String>,Boolean>();
        keyTemp =  new ArrayList<String>();
    }
    
    @PostConstruct
    public void initTable(){
        table.addAll(header.getTable());
        for(String t:table){
            List l = new ArrayList();
            l.addAll(header.getColumn(t));
            column.put(t, l);
        }
    }

    public HeaderFacade getHeader() {
        return header;
    }

    public void setHeader(HeaderFacade header) {
        this.header = header;
    }

    public List<String> getTable() {
        return table;
    }

    public void setTable(List<String> table) {
        this.table = table;
    }

    public Map<String, List<String>> getColumn() {
        return column;
    }

    public void setColumn(Map<String, List<String>> column) {
        this.column = column;
    }

    public List<String> getKeyTemp() {
        return keyTemp;
    }

    public void setKeyTemp(List<String> keyTemp) {
        this.keyTemp = keyTemp;
    }

    public Map<List<String>, Boolean> getCheck() {
        return check;
    }

    public void setCheck(Map<List<String>, Boolean> check) {
        this.check = check;
    }
    
    public List<String> getKey(String table, String col){
        keyTemp =  new ArrayList<String>();
        keyTemp.add(table);
        keyTemp.add(col);
        return keyTemp;
    }

    public Boolean listEmpty(String table){
        Boolean b = true;
        List<String> colList = column.get(table);
        for(String c:colList){
            if(check.get(getKey(table,c))){
                b = false;
            }
        }
        return b;
    }
    
    public String showTable(){
        WriteFile wf = new WriteFile();
        wf.delete("src/java/data");
        wf.delete("src/java/facade");
        wf.delete("src/java/list/concreteList");
        wf.delete("web/table");
        wf.delete("web/WEB-INF/csv");
        wf.writeDataXhtmlBegin();
        wf.writeDownloadBegin();
        int cptTab=1;
        for(Entry<String, List<String>> entry : column.entrySet()) {
            String t = entry.getKey();
            List<String> columnList = entry.getValue();
        
            String id = header.getPrimaryKey(t);
            int cptCol=1;   
            boolean empty = listEmpty(t); 
            
            for(String col:columnList){
                if(!empty && id.equals(col)){
                    wf.writeIframe(cptTab, t);
                    wf.writeDataJavaBegin(cptTab, t, id);
                    wf.writeFacade(cptTab);
                    wf.writeListJava(cptTab);
                    wf.writeTabBegin(cptTab);
                    if(check.get(getKey(t, col))){
                       wf.writeTabColBegin(cptTab);
                       wf.writeTabId(cptTab, col);
                    }
                }
                else if(check.get(getKey(t, col))){
                        wf.writeDataJavaColumn(cptTab,col,cptCol);
                        wf.writeTabColBegin(cptTab);
                        wf.writeTabCol(cptTab, col, cptCol);
                        cptCol++;
                }     
            }
            if(!empty){
                wf.writeDataJavaListBegin(cptTab);
                for(int k=1;k<cptCol;k++){
                    wf.writeDataJavaList(cptTab,k);
                }
                wf.writeDataJavaListEnd(cptTab);
                wf.writeDataJavaEnd(cptTab);
                wf.writeTabEnd(cptTab);
                cptTab++;
            }
        }
        for(int i=1;i<cptTab;i++){
           wf.writeDownloadImport(i);
        }
        wf.writeDownloadServletBegin();
        for(int i=1;i<cptTab;i++){
            wf.writeDownloadEJB(i);
        }
        wf.writeDownloadFill();
        for(int i=1;i<cptTab;i++){
            wf.writeDownloadFill2(i);
        }
        wf.writeDownloadEnd();
        wf.writeDataXhtmlEnd();
        return "case2";
    }
    
    public String next(){
        return "case3";
    }
}