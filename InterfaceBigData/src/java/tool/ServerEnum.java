package tool;

public enum ServerEnum {
    MYSQL("mysql://","com.mysql.jdbc.jdbc2.optional.MysqlDataSource"),
    PGSQL("derby://","org.apache.derby.jdbc.ClientDataSource"),
    ORACLE("","");
    
    private final String driver;
    private final String classname;

    private ServerEnum(String driver, String classname) {
        this.driver = driver;
        this.classname = classname;
    }

    public String getDriver() {
        return driver;
    }
    
    public String getClassname() {
        return classname;
    }
}
