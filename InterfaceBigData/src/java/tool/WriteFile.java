package tool;

import tool.ServerEnum;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WriteFile {
    private String address;

    public String getAddress() {
        return address;
    }
   
    public WriteFile() {
        this.address = "/home/anna/NetBeansProjects/InterfaceBigData/";
    }
    
    private void write(String addAdress,String text, Boolean bool){
        BufferedWriter output = null;
        try{
            output = new BufferedWriter(new FileWriter(address+addAdress, bool));
            output.write(text);
            
	}catch(IOException ioe){
            ioe.printStackTrace();
        }finally{
            try {
                output.flush();
                output.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private String constructJndi(String nameDb){
        return "jdbc/"+nameDb + "Jndi";
    }
    
    public void writeRessources(String serverDb, String ip, String port, String nameDb, String user, String password) {
       ServerEnum driver = ServerEnum.valueOf(serverDb);
       write("setup/glassfish-resources.xml","<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                    + "<!DOCTYPE resources PUBLIC \"-//GlassFish.org//DTD GlassFish Application Server 3.1 Resource Definitions//EN\" \"http://glassfish.org/dtds/glassfish-resources_1_5.dtd\">\n"
                    + "<resources>\n"
                    + "\t<jdbc-resource enabled=\"true\" jndi-name=\""+constructJndi(nameDb)+"\" object-type=\"user\" pool-name=\""+"Pool"+nameDb+"\">\n" 
                    + "\t\t<description/>\n" 
                    + "\t</jdbc-resource>\n"
                    + "\t<jdbc-connection-pool allow-non-component-callers=\"false\" associate-with-thread=\"false\" connection-creation-retry-attempts=\"0\" connection-creation-retry-interval-in-seconds=\"10\" connection-leak-reclaim=\"false\" connection-leak-timeout-in-seconds=\"0\" connection-validation-method=\"auto-commit\" datasource-classname=\""+driver.getClassname()+"\" fail-all-connections=\"false\" idle-timeout-in-seconds=\"300\" is-connection-validation-required=\"false\" is-isolation-level-guaranteed=\"true\" lazy-connection-association=\"false\" lazy-connection-enlistment=\"false\" match-connections=\"false\" max-connection-usage-count=\"0\" max-pool-size=\"32\" max-wait-time-in-millis=\"60000\" name=\""+"Pool"+nameDb+"\" non-transactional-connections=\"false\" ping=\"false\" pool-resize-quantity=\"2\" pooling=\"true\" res-type=\""+"javax.sql.DataSource"+"\" statement-cache-size=\"0\" statement-leak-reclaim=\"false\" statement-leak-timeout-in-seconds=\"0\" statement-timeout-in-seconds=\"-1\" steady-pool-size=\"8\" validate-atmost-once-period-in-seconds=\"0\" wrap-jdbc-objects=\"true\">\n"
                    + "\t\t<property name=\"URL\" value=\"jdbc:"+driver.getDriver()+ip+":"+port+"/"+nameDb+"\"/>\n"
                    + "\t\t<property name=\"serverName\" value=\""+ip+"\"/>\n" 
                    + "\t\t<property name=\"PortNumber\" value=\""+port+"\"/>\n" 
                    + "\t\t<property name=\"DatabaseName\" value=\""+nameDb+"\"/>\n" 
                    + "\t\t<property name=\"User\" value=\""+user+"\"/>\n" 
                    + "\t\t<property name=\"Password\" value=\""+password+"\"/>\n" 
                    + "\t</jdbc-connection-pool>\n"
                    + "</resources>",false);
    }
    
    public void writePersistence(String nameDb) { 
        write("src/conf/persistence.xml","<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "\t<persistence version=\"2.1\" xmlns=\"http://xmlns.jcp.org/xml/ns/persistence\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd\">\n" +
            "\t<persistence-unit name=\"InterfaceBigDataPU\" transaction-type=\"JTA\">\n" +
            "\t\t<jta-data-source>"+constructJndi(nameDb)+"</jta-data-source>\n" +
            "\t\t<exclude-unlisted-classes>false</exclude-unlisted-classes>\n" +
            "\t\t<properties/>\n" +
            "\t</persistence-unit>\n" +
            "</persistence>",false);
    }
    
    public void writeDataJavaBegin(int i,String table,String id){
        write("src/java/data/Data"+i+".java","package data;\n" +
                                                "\n" +
                                                "import java.io.Serializable;\n" +
                                                "import java.util.ArrayList;\n"+
                                                "import java.util.List;\n"+
                                                "import javax.persistence.Column;\n" +
                                                "import javax.persistence.Entity;\n" +
                                                "import javax.persistence.Id;\n" +
                                                "import javax.persistence.Table;\n" +
                                                "\n" +
                                                "@Entity\n"+
                                                "@Table(name = \""+table+"\")\n" +
                                                "public class Data"+i+" implements Serializable{\n" +
                                                "\t@Column(name = \""+id+"\")\n" +
                                                "\t@Id\n" +
                                                "\tprivate String id;\n\n" +
                                                "\tpublic Data"+i+"() {\n" +
                                                "\t}\n\n"+
                                                "\tpublic String getId() {\n" +
                                                "\t\treturn id;\n" +
                                                "\t}\n\n"+
                                                "\tpublic void setId(String id) {\n" +
                                                "\t\tthis.id = id;\n" +
                                                "\t}\n\n",false);
    }
    
    public void writeDataJavaColumn(int j, String column, int i){
        write("src/java/data/Data"+j+".java","\t@Column(name = \""+column+"\")\n" +
                                                "\tprivate String item"+i+";\n" +
                                                "\n" +
                                                "\tpublic String getItem"+i+"() {\n" +
                                                "\t\treturn item"+i+";\n" +
                                                "\t}\n" +
                                                "\n" +
                                                "\tpublic void setItem"+i+"(String item"+i+") {\n" +
                                                "\t\tthis.item"+i+" = item"+i+";\n" +
                                                "\t}\n\n",true);
    }
    
    public void writeDataJavaListBegin(int i){
        write("src/java/data/Data"+i+".java","\tpublic List<String> getItems(){\n" +
                                        "\t\tList<String> l = new ArrayList<String>();\n", true);
    }
    
    public void writeDataJavaList(int j, int i){
        write("src/java/data/Data"+j+".java","\t\tl.add(item"+i+");\n",true);
    }
    
    public void writeDataJavaListEnd(int i){
        write("src/java/data/Data"+i+".java","\t\treturn l;\n"+
                                        "\t}\n",true);
    }
    
    public void writeDataJavaEnd(int i){
        write("src/java/data/Data"+i+".java","}",true);
    }
    
    public void writeFacade(int i){
        write("src/java/facade/DataFacade"+i+".java","package facade;\n" +
            "\n" +
            "import data.Data"+i+";\n" +
            "import unit.AbstractFacade;\n"+
            "import javax.ejb.Stateless;\n" +
            "import javax.persistence.EntityManager;\n" +
            "import javax.persistence.PersistenceContext;\n" +
            "\n" +
            "@Stateless\n" +
            "public class DataFacade"+i+" extends AbstractFacade<Data"+i+">{\n" +
            "\t@PersistenceContext(unitName = \"InterfaceBigDataPU\")\n" +
            "\tprivate EntityManager em;\n" +
            "\n"+ 
            "\t@Override\n" +
            "\tprotected EntityManager getEntityManager() {\n" +
            "\t\treturn em;\n" +
            "\t}\n" +
            "\n" +
            "\tpublic DataFacade"+i+"() {\n" +
            "\t\tsuper(Data"+i+".class);\n" +
            "\t}\n" +
            "}",false);
    }
    
    public void writeListJava(int i){
        write("src/java/list/concreteList/DataList"+i+".java","package list.concreteList;\n" +
            "\n" +
            "import data.Data"+i+";\n" +
            "import facade.DataFacade"+i+";\n" +
            "import javax.annotation.PostConstruct;\n" +
            "import javax.ejb.EJB;\n" +
            "import javax.faces.bean.ManagedBean;\n"+ 
            "import javax.faces.bean.ApplicationScoped;\n" +
            "import list.AbstractList;\n" +
            "\n" +
            "@ManagedBean\n" +
            "@ApplicationScoped\n" +
            "public class DataList"+i+" extends AbstractList<Data"+i+">{\n" +
            "\t@EJB\n" +
            "\tDataFacade"+i+" df;\n" +
            "\n" +
            "\tpublic DataList"+i+"() {\n" +
            "\t\tsuper();\n" +
            "\t}\n" +
            "\n" +
            "\t@PostConstruct\n" +
            "\tpublic void addData(){\n" +
            "\t\tsetL(df.findAll());\n" +
            "\t}\n" +
            "}",false);
    }
    
    public void writeDataXhtmlBegin(){
        write("web/data.xhtml","<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<!DOCTYPE html><html lang=\"en\"\n" +
                    "\txmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                    "\txmlns:h=\"http://xmlns.jcp.org/jsf/html\"\n" +
                    "\txmlns:f=\"http://java.sun.com/jsf/core\">\n" +
                    "\n" +
                    "<h:head>\n" +
                    "\t<meta charset=\"utf-8\"/>\n" +
                    "\t<title>Interface for big data</title>\n" +
                    "\t<link rel=\"stylesheet\" href=\"css/style.css\" />\n" +
                    "</h:head>\n" +
                    "\n" +
                    "<h:body>\n" +
                    "\n" +
                    "\t<!-- navigation section -->\n" +
                    "\t<section>\n" +
                    "\t\t<div class=\"navigation\">\n" +
                    "\t\t\t<ul>\n" +
                    "\t\t\t\t<li><a href=\"index.html\">HOME</a></li>\n" +
                    "\t\t\t\t<li><a href=\"about.html\">ABOUT</a></li>\n" +
                    "\t\t\t\t<li><a href=\"connect1.xhtml\">CONNECT</a></li>\n" +
                    "\t\t\t\t<li><a class=\"blutext\" href=\"data.xhtml\">SEE DATA</a></li>\n" +
                    "\t\t\t\t<li><a href=\"contact.html\">CONTACT</a></li>\n" +
                    "\t\t\t</ul>\n" +
                    "\t\t</div>\n" +
                    "\t</section>\n" +
                    "\n" +
                    "\t<!-- data section -->\n" +
                    "\t<f:view>\n" +
                    "\t\t<section id=\"data\">\n" +
                    "\t\t\t<h1 class=\"heading bold\">SEE DATA</h1>\n" +
                    "\t\t\t<hr/>\n" +
                    "\t\t\t<div class=\"container\">\n",false);
    }
    
    public void writeIframe(int i, String table){    
        write("web/data.xhtml","\t\t\t\t<div class=\"lien\">\n"
                + "\t\t\t\t\t<h2>"+table+"</h2>\n"
                + "\t\t\t\t\t<a href=\"/InterfaceBigData/csv/data_"+table+".csv?table="+table+"&amp;id="+i+"\">\n"
                + "\t\t\t\t\t\t<iframe width=\"350px\" height=\"200px\" src=\"table\\tab"+i+".xhtml\"/>\n"
                + "\t\t\t\t\t</a>\n" 
                +"\t\t\t\t</div>\n",true);
    }
    
    public void writeDataXhtmlEnd(){ 
        write("web/data.xhtml","\t\t\t</div>\n" +
                "\t\t</section>\n" +
                "\t</f:view>\n" +
                "\n" +
                "</h:body>\n" +
                "</html>",true);
    }
    
    public void writeTabBegin(int i){
        write("web/table/tab"+i+".xhtml","<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<!DOCTYPE html><html lang=\"en\"\n" +
                    "\txmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                    "\txmlns:h=\"http://xmlns.jcp.org/jsf/html\"\n" +
                    "\txmlns:f=\"http://java.sun.com/jsf/core\">\n" +
                    "\n" +
                    "<h:head>\n" +
                    "\t<meta charset=\"utf-8\"/>\n" +
                    "\t<title>Interface for big data</title>\n" +
                    "\t<link rel=\"stylesheet\" href=\"css/style.css\" />\n" +
                    "</h:head>\n" +
                    "\n" +
                    "<h:body>\n" +
                    "\t<h:dataTable value=\"#{dataList"+i+".l}\" var=\"item\">\n", false);
    }
    
    public void writeTabId(int i, String col){
         write("web/table/tab"+i+".xhtml","\t\t\t<f:facet name=\"header\">"+col+"</f:facet>\n" +
                "\t\t\t<h:outputText value=\"#{item.id}\"/>\n" +
                "\t\t</h:column>\n",true);
    }
    
    public void writeTabCol(int i, String col, int j){
        write("web/table/tab"+i+".xhtml","\t\t\t<f:facet name=\"header\">"+col+"</f:facet>\n" +
                "\t\t\t<h:outputText value=\"#{item.item"+j+"}\"/>\n" +
                "\t\t</h:column>\n",true);
    }
    
    public void writeTabColBegin(int i){
        write("web/table/tab"+i+".xhtml","\t\t<h:column>\n",true);
    }
    
    public void writeTabEnd(int i){
         write("web/table/tab"+i+".xhtml","\t</h:dataTable>"+
                "\n" +
                "</h:body>\n"+ 
                 "</html>",true);
    }
    
    public void writeDownloadBegin(){
        write("src/java/servlet/Download.java","package servlet;\n" +
            "\n" +
            "import java.io.BufferedInputStream;\n" +
            "import java.io.BufferedOutputStream;\n" +
            "import java.io.BufferedWriter;\n" +
            "import java.io.File;\n" +
            "import java.io.FileInputStream;\n" +
            "import java.io.FileWriter;\n" +
            "import java.io.IOException;\n" +
            "import java.util.ArrayList;\n" +
            "import java.util.List;\n" +
            "import java.util.logging.Level;\n" +
            "import java.util.logging.Logger;\n" +
            "import javax.ejb.EJB;\n" +
            "import javax.servlet.ServletException;\n" +
            "import javax.servlet.annotation.WebServlet;\n" +
            "import javax.servlet.http.HttpServlet;\n" +
            "import javax.servlet.http.HttpServletRequest;\n" +
            "import javax.servlet.http.HttpServletResponse;\n" +
            "import tool.WriteFile;\n", false);
    }
    
    public void writeDownloadImport(int i){
        write("src/java/servlet/Download.java","import data.Data"+i+";\n" +
                "import facade.DataFacade"+i+";\n", true);
    }
    
    public void writeDownloadServletBegin(){
        write("src/java/servlet/Download.java","\n@WebServlet(\"/csv/*\")\n" +
            "public class Download extends HttpServlet {\n" +
            "\tpublic static final int TAMPON = 10240; // 10ko\n" +
            "\n" +
            "\tprivate List<String> ls;\n" +
            "\n", true);
    }
    
    public void writeDownloadEJB(int i){
        write("src/java/servlet/Download.java","\t@EJB\n" +
            "\tDataFacade"+i+" df"+i+";\n" +
            "\tprivate void row"+i+"(){\n" +
                "\t\tls = new ArrayList<String>();\n" +
                "\t\tList<Data"+i+"> ld = df"+i+".findAll();\n" +
                "\t\tfor(Data"+i+" d:ld){\n" +
                "\t\t\tls.add(d.getId());\n" +
                "\t\t\tls.add(\";\");\n" +
                "\t\t\tfor(String s:d.getItems()){\n" +
                "\t\t\t\tls.add(s);\n" +
                "\t\t\t\tls.add(\";\");\n" +
                "\t\t\t}\n" +
                "\t\t\tls.add(\"\\n\");\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n",true);
    }

    public void writeDownloadFill(){
        write("src/java/servlet/Download.java","\tprivate void fillList(String i){\n" +
                "\t\tswitch(i){\n",true);
    }

    public void writeDownloadFill2(int i){
        write("src/java/servlet/Download.java","\t\t\tcase \""+i+"\":\n" +
            "\t\t\t\trow"+i+"();\n" +
            "\t\t\t\tbreak;\n",true);
    }
    
    public void writeDownloadEnd(){
        write("src/java/servlet/Download.java","\t\t\tdefault:\n" +
            "\t\t\t\tbreak;\n" +
            "\t\t}\n" +
            "\t}\n" +
            "\n" +
            "\tpublic void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {\n" +
            "\t\tString way = \"/home/anna/NetBeansProjects/InterfaceBigData/web/WEB-INF/csv/\";\n" +
            "\t\tString table = request.getParameter(\"table\");\n" +
            "\t\tBufferedWriter output = null;\n" +
            "\t\ttry{\n" +
            "\t\t\toutput = new BufferedWriter(new FileWriter(way+\"data_\"+table+\".csv\",false));\n" +
            "\t\t\tfillList(request.getParameter(\"id\"));\n" +
            "\t\t\tfor(String w:ls){\n" +
            "\t\t\t\toutput.write(w);\n" +
            "\t\t\t}\n" +
            "\t\t\toutput.write(\"\\n\");\n" +
            "\n" +
            "\n" +
            "\t\t}catch(IOException ioe){\n" +
            "\t\t\tioe.printStackTrace();\n" +
            "\t\t}finally{\n" +
            "\t\t\ttry {\n" +
            "\t\t\t\toutput.flush();\n" +
            "\t\t\t\toutput.close();\n" +
            "\t\t\t} catch (IOException ex) {\n" +
            "\t\t\t\tLogger.getLogger(WriteFile.class.getName()).log(Level.SEVERE, null, ex);\n" +
            "\t\t\t}\n" +
            "\t\t}\n" +
            "\t\tFile file = new File(way+\"data_\"+table+\".csv\");\n" +
            "\t\tif ( !file.exists() ) {\n" +
            "\t\t\tresponse.sendError( HttpServletResponse.SC_NOT_FOUND );\n" +
            "\t\t\treturn;\n" +
            "\t\t}\n" +
            "\t\tresponse.reset();\n" +
            "\t\tresponse.setBufferSize(TAMPON );\n" +
            "\t\tresponse.setContentType( \"csv\" );\n" +
            "\t\tresponse.setHeader( \"Content-Length\", String.valueOf( file.length() ) );\n" +
            "\t\tresponse.setHeader( \"Content-Disposition\", \"attachment; filename=\\\"\" + file.getName() + \"\\\"\" );\n" +
            "\t\tBufferedInputStream inputStream = null;\n" +
            "\t\tBufferedOutputStream outputStream = null;\n" +
            "\t\ttry {\n" +
            "\t\t\tinputStream = new BufferedInputStream( new FileInputStream( file ), TAMPON );\n" +
            "\t\t\toutputStream = new BufferedOutputStream( response.getOutputStream(), TAMPON );\n" +
            "\t\t\tbyte[] tampon = new byte[TAMPON];\n" +
            "\t\t\tint longueur;\n" +
            "\t\t\twhile ( ( longueur = inputStream.read( tampon ) ) > 0 ) {\n" +
            "\t\t\t\toutputStream.write( tampon, 0, longueur );\n" +
            "\t\t\t}\n" +
            "\t\t} finally {\n" +
            "\t\t\toutputStream.close();\n" +
            "\t\t\tinputStream.close();\n" +
            "\t\t}\n" +
            "\t}\n" +
            "}",true);
    }
    
    public void delete(String a){
        File path = new File(address+a);
        if( path.exists() ){
            File[] files = path.listFiles();
            for(File f:files ){
                f.delete();
            }
        }
    }
}
