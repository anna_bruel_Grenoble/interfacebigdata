package servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tool.WriteFile;
import data.Data1;
import facade.DataFacade1;

@WebServlet("/csv/*")
public class Download extends HttpServlet {
	public static final int TAMPON = 10240; // 10ko

	private List<String> ls;

	@EJB
	DataFacade1 df1;
	private void row1(){
		ls = new ArrayList<String>();
		List<Data1> ld = df1.findAll();
		for(Data1 d:ld){
			ls.add(d.getId());
			ls.add(";");
			for(String s:d.getItems()){
				ls.add(s);
				ls.add(";");
			}
			ls.add("\n");
		}
	}

	private void fillList(String i){
		switch(i){
			case "1":
				row1();
				break;
			default:
				break;
		}
	}

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		String way = "/home/anna/NetBeansProjects/InterfaceBigData/web/WEB-INF/csv/";
		String table = request.getParameter("table");
		BufferedWriter output = null;
		try{
			output = new BufferedWriter(new FileWriter(way+"data_"+table+".csv",false));
			fillList(request.getParameter("id"));
			for(String w:ls){
				output.write(w);
			}
			output.write("\n");


		}catch(IOException ioe){
			ioe.printStackTrace();
		}finally{
			try {
				output.flush();
				output.close();
			} catch (IOException ex) {
				Logger.getLogger(WriteFile.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		File file = new File(way+"data_"+table+".csv");
		if ( !file.exists() ) {
			response.sendError( HttpServletResponse.SC_NOT_FOUND );
			return;
		}
		response.reset();
		response.setBufferSize(TAMPON );
		response.setContentType( "csv" );
		response.setHeader( "Content-Length", String.valueOf( file.length() ) );
		response.setHeader( "Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" );
		BufferedInputStream inputStream = null;
		BufferedOutputStream outputStream = null;
		try {
			inputStream = new BufferedInputStream( new FileInputStream( file ), TAMPON );
			outputStream = new BufferedOutputStream( response.getOutputStream(), TAMPON );
			byte[] tampon = new byte[TAMPON];
			int longueur;
			while ( ( longueur = inputStream.read( tampon ) ) > 0 ) {
				outputStream.write( tampon, 0, longueur );
			}
		} finally {
			outputStream.close();
			inputStream.close();
		}
	}
}