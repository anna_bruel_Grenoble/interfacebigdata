package data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SHOPPINGCARTITEM")
public class Data1 implements Serializable{
	@Column(name = "ID")
	@Id
	private String id;

	public Data1() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "USER_LOGIN")
	private String item1;

	public String getItem1() {
		return item1;
	}

	public void setItem1(String item1) {
		this.item1 = item1;
	}

	@Column(name = "PRODUCT_ID")
	private String item2;

	public String getItem2() {
		return item2;
	}

	public void setItem2(String item2) {
		this.item2 = item2;
	}

	@Column(name = "QUANTITY")
	private String item3;

	public String getItem3() {
		return item3;
	}

	public void setItem3(String item3) {
		this.item3 = item3;
	}

	public List<String> getItems(){
		List<String> l = new ArrayList<String>();
		l.add(item1);
		l.add(item2);
		l.add(item3);
		return l;
	}
}