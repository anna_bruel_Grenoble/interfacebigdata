package unit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class HeaderFacade extends AbstractFacade<Header> {
    @PersistenceContext(unitName = "InterfaceBigDataPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public HeaderFacade() {
        super(Header.class);
    }
    
    public List<String> getTable(){
        List<String> l = new ArrayList<String>();
        try {
            Connection con = getEntityManager().unwrap(Connection.class);
            ResultSet tables = con.getMetaData().getTables(con.getCatalog(),null,"%",null);
            while(tables.next()){
                l.add(tables.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HeaderFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;
    }
    
    public List<String> getColumn(String table){
        List<String> l = new ArrayList<String>();
        try {
            Connection con = getEntityManager().unwrap(Connection.class);
            ResultSet col = con.getMetaData().getColumns(con.getCatalog(), null, table, "%");
            while(col.next()){
                l.add(col.getString(4));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HeaderFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;
    }
    
    public String getPrimaryKey(String table){
        try {
            Connection con = getEntityManager().unwrap(Connection.class);
            ResultSet key = con.getMetaData().getPrimaryKeys(con.getCatalog(), null, table);
            if(key.next())
                return key.getString("COLUMN_NAME");
        } catch (SQLException ex) {
            Logger.getLogger(HeaderFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
