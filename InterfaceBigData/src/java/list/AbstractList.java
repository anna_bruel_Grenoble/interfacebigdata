package list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AbstractList<T> implements Serializable{
    protected List<T> l;
    
    public AbstractList() {
        this.l = new ArrayList<T>();
    }

    public List<T> getL() {
	return l;
    }

    public void setL(List<T> l) {
	this.l = l;
    }
}