package list.concreteList;

import data.Data1;
import facade.DataFacade1;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import list.AbstractList;

@ManagedBean
@ApplicationScoped
public class DataList1 extends AbstractList<Data1>{
	@EJB
	DataFacade1 df;

	public DataList1() {
		super();
	}

	@PostConstruct
	public void addData(){
		setL(df.findAll());
	}
}